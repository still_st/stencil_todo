// todo: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './todo.core.js';
import {
  TaskApp,
  TimerApp,
  TodoApp
} from './todo.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    TaskApp,
    TimerApp,
    TodoApp
  ], opts);
}