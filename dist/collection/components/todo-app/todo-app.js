export class TodoApp {
    constructor() {
        this.tasks = [];
    }
    componentWillUpdate() {
        console.log('TodoApp will update and re-render');
    }
    handleSubmit(e) {
        e.preventDefault();
        if (this.newTask) {
            this.addTask(this.newTask);
        }
        this.newTask = "";
    }
    handleChange(e) {
        this.newTask = e.target.value;
    }
    addTask(task) {
        this.tasks = [...this.tasks, { "id": this.tasks.length, "name": task, "status": "active" }];
    }
    finishTask(taskId) {
        let deleteIndex = -1;
        let taskText = "";
        this.tasks.forEach(function (item, i) {
            if (item.id == taskId) {
                deleteIndex = i;
                taskText = item.name;
            }
        });
        if (deleteIndex > -1) {
            this.tasks.splice(deleteIndex, 1);
            this.tasks = [...this.tasks, { "id": taskId, "name": taskText, "status": "finished" }];
        }
    }
    taskDeleteHandler(event) {
        this.DeleteTask(event.detail.id);
        console.log('Received the custom taskDelete event: ', event.detail);
    }
    taskFinishedHandler(event) {
        this.finishTask(event.detail.id);
        console.log('Received the custom taskFinished event: ', event.detail);
    }
    DeleteTask(taskId) {
        let deleteIndex = -1;
        this.tasks.forEach(function (item, i) {
            if (item.id == taskId) {
                deleteIndex = i;
            }
        });
        if (deleteIndex > -1) {
            this.tasks.splice(deleteIndex, 1);
            /*Иначе не происходит обновление компонента*/
            this.tasks = [...this.tasks];
        }
    }
    addClass(className) {
        this.todoApp.classList.add(className);
    }
    // Для newTask не нужен
    // @Watch('newTask')
    // validateName(newValue: string) {
    //   console.log("inside watch");
    //   const isBlank = typeof newValue == null;
    //
    //   if (isBlank) {
    //     console.log("not today");
    //     throw new Error('name: required')
    //   };
    //
    // }
    /*Типа API*/
    showActiveTasks() {
        for (let task of this.tasks) {
            if (task.status == "active") {
                console.log(task.name);
            }
        }
    }
    showFinishedTasks() {
        for (let task of this.tasks) {
            if (task.status == "finished") {
                console.log(task.name);
            }
        }
    }
    addActiveTask(task) {
        if (task) {
            this.addTask(task);
        }
    }
    finishActiveTask(task) {
        if (task !== "") {
            this.finishTask(task);
        }
    }
    addComponentClass(className) {
        if (className) {
            this.addClass(className);
        }
    }
    handleKeyDown(ev) {
        console.log('keydown');
        if (ev.keyCode === 40) {
            console.log('down arrow pressed');
        }
    }
    render() {
        return (h("div", { class: "todo__wrap" },
            h("slot", null),
            h("timer-app", null,
                h("span", { class: "timer__slot" })),
            h("h1", { class: "todo__title" }, "Simple Todo"),
            h("form", { class: "todo__form", onSubmit: (e) => this.handleSubmit(e) },
                h("label", { class: "todo__form-label" },
                    h("input", { class: "todo__form-input", placeholder: "\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0437\u0430\u0434\u0430\u0447\u0438", type: "text", value: this.newTask, onInput: (e) => this.handleChange(e) })),
                h("input", { class: "todo__form-submit", type: "submit", value: "\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C" })),
            this.tasks.filter(function (task) { return task.status == "active"; }).length > 0 &&
                h("div", { class: "todo__tasks todo__tasks--active" },
                    h("h2", { class: "todo__title todo__title--small" }, "\u0410\u043A\u0442\u0438\u0432\u043D\u044B\u0435 \u0437\u0430\u0434\u0430\u0447\u0438"),
                    this.tasks.filter(function (task) { return task.status == "active"; }).map((task) => h("task-app", { taskId: task.id, taskText: task.name, taskStatus: "active" }))),
            this.tasks.filter(function (task) { return task.status == "finished"; }).length > 0 &&
                h("div", { class: "todo__tasks todo__tasks--finished" },
                    h("h2", { class: "todo__title todo__title--small" }, "\u0412\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u043D\u044B\u0435 \u0437\u0430\u0434\u0430\u0447\u0438"),
                    this.tasks.filter(function (task) { return task.status == "finished"; }).map((task) => h("task-app", { taskId: task.id, taskText: task.name, taskStatus: "finished" })))));
    }
    static get is() { return "todo-app"; }
    static get encapsulation() { return "shadow"; }
    static get host() { return {
        "theme": "todo"
    }; }
    static get properties() { return {
        "addActiveTask": {
            "method": true
        },
        "addComponentClass": {
            "method": true
        },
        "finishActiveTask": {
            "method": true
        },
        "newTask": {
            "type": String,
            "attr": "new-task",
            "mutable": true
        },
        "showActiveTasks": {
            "method": true
        },
        "showFinishedTasks": {
            "method": true
        },
        "tasks": {
            "state": true
        },
        "todoApp": {
            "elementRef": true
        }
    }; }
    static get listeners() { return [{
            "name": "taskDelete",
            "method": "taskDeleteHandler"
        }, {
            "name": "taskFinished",
            "method": "taskFinishedHandler"
        }, {
            "name": "keydown",
            "method": "handleKeyDown"
        }]; }
    static get style() { return "/**style-placeholder:todo-app:**/"; }
}
