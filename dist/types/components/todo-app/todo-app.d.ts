import '../../stencil.core';
export declare class TodoApp {
    newTask: string;
    tasks: Array<{
        "id": number;
        "name": string;
        "status": string;
    }>;
    componentWillUpdate(): void;
    handleSubmit(e: any): void;
    handleChange(e: any): void;
    addTask(task: string): void;
    finishTask(taskId: number): void;
    taskDeleteHandler(event: CustomEvent): void;
    taskFinishedHandler(event: CustomEvent): void;
    DeleteTask(taskId: number): void;
    todoApp: HTMLElement;
    addClass(className: any): void;
    showActiveTasks(): void;
    showFinishedTasks(): void;
    addActiveTask(task: any): void;
    finishActiveTask(task: any): void;
    addComponentClass(className: any): void;
    handleKeyDown(ev: any): void;
    render(): JSX.Element;
}
