import { Component, State } from '@stencil/core';

@Component({
  tag: 'timer-app',
  styleUrl: 'timer-app.scss',
  host: {
    theme: 'timer'
  },
  shadow: true
})

export class TimerApp {

  timer: number;

  @State() time: number = Date.now();

  componentDidLoad() {
    this.timer = window.setInterval(() => {
      this.time = Date.now();
    }, 1000);
  }

  componentDidUnload() {
    clearInterval(this.timer);
  }

  render() {
    const time = new Date(this.time).toLocaleTimeString();

    return (
        <div class="timer__wrap">
           <slot/>
            <span>{ time }</span>
            <div class="timer__extra">

            </div>
        </div>
    );
  }
}
