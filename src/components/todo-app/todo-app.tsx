import { Component, Prop, State, Method, Element, Listen } from '@stencil/core';

@Component({
  tag: 'todo-app',
  styleUrl: 'todo-app.scss',
  host: {
    theme: 'todo'
  },
  shadow: true
})

export class TodoApp {

  @Prop({ mutable: true }) newTask: string;
  @State() tasks: Array <{ "id":number, "name": string, "status": string }> = [];

  componentWillUpdate() {
    console.log('TodoApp will update and re-render');
  }

  handleSubmit(e) {
    e.preventDefault();

    if(this.newTask) {
      this.addTask(this.newTask);
    }

    this.newTask = "";
  }

  handleChange(e) {
    this.newTask = e.target.value;
  }

  addTask(task: string) {
    this.tasks = [...this.tasks, {"id": this.tasks.length, "name": task, "status": "active"}];
  }


  finishTask(taskId: number) {

    let deleteIndex = -1;
    let taskText = "";

    this.tasks.forEach(function(item, i) {
      if (item.id == taskId){
        deleteIndex = i;
        taskText = item.name;
      }
    });

    if (deleteIndex > -1) {
      this.tasks.splice(deleteIndex, 1);

      this.tasks = [...this.tasks, {"id": taskId, "name": taskText, "status": "finished"}];
    }

  }

  @Listen('taskDelete')
  taskDeleteHandler(event: CustomEvent) {
    this.DeleteTask(event.detail.id);
    console.log('Received the custom taskDelete event: ', event.detail);
  }

  @Listen('taskFinished')
  taskFinishedHandler(event: CustomEvent) {
    this.finishTask(event.detail.id);
    console.log('Received the custom taskFinished event: ', event.detail);
  }


  DeleteTask(taskId: number) {
    let deleteIndex = -1;

    this.tasks.forEach(function(item, i) {
      if(item.id == taskId){
        deleteIndex = i;
      }
    });

    if(deleteIndex > -1) {
      this.tasks.splice(deleteIndex, 1);

      /*Иначе не происходит обновление компонента*/
      this.tasks = [...this.tasks];
    }
  }

  @Element() todoApp: HTMLElement;

  addClass(className){
   this.todoApp.classList.add(className);
  }

  // Для newTask не нужен
  // @Watch('newTask')
  // validateName(newValue: string) {
  //   console.log("inside watch");
  //   const isBlank = typeof newValue == null;
  //
  //   if (isBlank) {
  //     console.log("not today");
  //     throw new Error('name: required')
  //   };
  //
  // }

  /*Типа API*/
  @Method()
  showActiveTasks() {
    for (let task of this.tasks) {
      if (task.status == "active")
      {
        console.log( task.name );
      }
    }
  }

  @Method()
  showFinishedTasks() {
    for (let task of this.tasks) {
      if (task.status == "finished")
      {
        console.log( task.name );
      }
    }
  }

  @Method()
  addActiveTask(task) {
    if(task) {
      this.addTask(task);
    }
  }

  @Method()
  finishActiveTask(task) {
    if(task !== "") {
      this.finishTask(task);
    }
  }

  @Method()
  addComponentClass(className) {
    if(className) {
      this.addClass(className);
    }
  }

  @Listen('keydown')
  handleKeyDown(ev){
    console.log('keydown');
    if(ev.keyCode === 40){
      console.log('down arrow pressed')
    }
  }


  render() {
    return (

        <div class="todo__wrap">
          <slot/>

          <timer-app>
            <span class="timer__slot"></span>
          </timer-app>

          <h1 class="todo__title">Simple Todo</h1>
          <form class="todo__form" onSubmit={(e) => this.handleSubmit(e)}>
            <label class="todo__form-label">
              <input class="todo__form-input" placeholder="Название задачи" type="text" value={this.newTask} onInput={(e) => this.handleChange(e)} />
            </label>
            <input class="todo__form-submit" type="submit" value="Добавить" />
          </form>

          {
            this.tasks.filter(function(task) { return task.status == "active"; }).length > 0 &&

            <div class="todo__tasks todo__tasks--active">
              <h2 class="todo__title todo__title--small">Активные задачи</h2>

              {
                this.tasks.filter(function(task) { return task.status == "active"; }).map((task) =>

                  <task-app taskId={task.id} taskText={task.name} taskStatus="active">
                  </task-app>
              )}
            </div>
          }

          {
            this.tasks.filter(function(task) { return task.status == "finished"; }).length > 0 &&

            <div class="todo__tasks todo__tasks--finished">
               <h2 class="todo__title todo__title--small">Выполненные задачи</h2>

              {
                this.tasks.filter(function(task) { return task.status == "finished"; }).map((task) =>

                <task-app taskId={task.id} taskText={task.name} taskStatus="finished">
                </task-app>

              )}
            </div>
          }
        </div>

    );
  }
}
