import { Component, Prop, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'task-app',
  styleUrl: 'task-app.scss',
  host: {
    theme: 'task'
  },
  shadow: true
})

export class TaskApp {

  @Prop() taskId: number;
  @Prop() taskText: string;
  @Prop() taskStatus: string;

  @Event() taskDelete: EventEmitter;
  @Event() taskFinished: EventEmitter;

  taskFinishedFire() {
    this.taskFinished.emit({id: this.taskId, name: this.taskText, status: this.taskStatus});
  }

  taskDeleteFire() {
    this.taskDelete.emit({id: this.taskId, name: this.taskText, status: this.taskStatus});
  }

  render() {
    return (

        <div class="task">
          <span class="task__text"> {this.taskText} </span>
          <div class="task__buttons">

            {
              this.taskStatus == "active" &&
                <button class="task__button task__button--done" onClick={() => this.taskFinishedFire()}>
                </button>
            }

            <button class="task__button task__button--del" onClick={() => this.taskDeleteFire()}>
            </button>

          </div>
        </div>

    );
  }
}
