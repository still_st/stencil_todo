## Getting Started

To start building a new web component using Stencil, clone this repo to a new directory:

```bash
git clone https://still_st@bitbucket.org/still_st/stencil_todo.git
cd stencil_todo
git remote rm origin
```

and run:

```bash
npm install
npm start
```
