const sass = require('@stencil/sass');

exports.config = {
  namespace: 'todo',
  plugins: [
    sass()
  ],


  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: false
    }
  ],
  globalStyle: ['src/global/global.scss']

};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
}
